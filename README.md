You have a lot of Raspberry Pis and want to have them grouped
as a Kubernetes cluster which is not possible due to the fact
that a Kubernetes installation is too heavyweight for a small
1 GB RPi 3. Rancher created a lightweight Kubernetes
platform called [K3s](https://k3s.io), which aims to be
installed on smaller ARM devices.

K3s can be installed on any Linux operating system but Rancher
also developed a lightweight operating systems for exact
that purpose: [K3os](https://k3os.io). This guide shows how
K3os can be installed on a RPi 3 (including Wifi support) and
following that the way more easier K3s installation.

![How K3s works](assets/how-it-works-k3s.svg)

## K3os

To keep things simple, after some researching I found
a [github repository](https://github.com/sgielen/picl-k3os-image-generator)
which addresses the generation of an image via script
that can be loaded on an µsdcard for a RPi. The needed files
are stored within the k3os-image-generator subfolder.

I describe the installation done with a Mac here. The
script `build-image.sh` runs solely on a Linux operating
system. So you have to use some virtualization software like
VirtualBox or Parallels.

Before you run the script the first time, create a YAML file
first within the subfolder `config` first named after the
MAC address of the devices eth0 interface 
(e.g. `b8:27:eb:34:b5:22.yaml`). The configuration
details can be found 
[here](https://github.com/rancher/k3os/blob/master/README.md)
but it could have the following content:

```yaml
hostname: k3os01
k3os:
  labels:
    region: de-east-1
  server_url: https://k3os01:6443
  password: thehashedpasswordofrancheruser
  wifi:
  - name: thenameoftheaccesspoint
    passphrase: theunhashedpassphrase
  modules:
  - brcmfmac
ssh_authorized_keys:
- ssh-rsa <somefirstkey> host1
- ssh-ed25519 <somesecondkey> host2
```

The hostname of the node will be `k3os01` and the
password of the rancher user should be set but
hashed with `openssl passwd -1`. The `server_url`
will be used if that node will join an existing
cluster as agent node. The node can also be labeled
with a region, for instance if you have multiple
homes and you want high availability in your cluster.
For example if at one of those places the internet
provider fails, the replicas of your services on the
other cluster region can still serve.

If some kernel modules should be loaded at start,
list the in `k3os.modules`, here for instance the
wifi module for a RPi3. Next to that you can
set the wifi access point name and its unhashed
passphrase to connect the node to the according
wireless network.

The script `build-image.sh` should be adjusted
before the first run at line 78 to load the newest
RPi firmware from https://github.com/raspberrypi/firmware/archive
and at line 93 to load the newest k3os release
from https://github.com/rancher/k3os/releases/download .

If you have a newer or older RPi you may have
to add download instructions (line 118) 
and copy instructions (line 283) for
the wifi device firmware. You can see what is
missing by connecting the device with ethernet
and check the system messages with `dmesg`.

If you run the script the first time with
`./build-image.sh raspberrypi` it may
fail due to missing commands that may have to be installed.
When the script successfully ran, there will be a
file like `picl-k3os-v0.9.1-raspberry.img` that
can be installed by finding the device node of the µsdcard first
with `diskutil list`.

Let's say the node is `/dev/sda3` you have to unmount that
device first with `diskutil unmountDisk /dev/disk3`. The
image is loaded with `dd`, like
`sudo dd bs=1m if=picl-k3os-v0.9.1-raspberrypi.img of=/dev/rdisk3 conv=sync`
. The `rdisk3` is no typo here. Eject the device with
`sudo diskutil eject /dev/rdisk3`.

After the first boot the wireless device may not come
up automatically. You have to connect via ethernet and use
[`connmanctl`](https://wiki.archlinux.org/index.php/ConnMan)
to connect to your access point just once. After that, the device
will come up automatically.

## K3s

According the [documentation](https://github.com/rancher/k3s) the
installation of k3s is like the one of homebrew a simple call of
a shell script:

```bash
curl -sfL https://get.k3s.io | sh -
```

That installs k3s and that node will be a single master node cluster.
After some time there will be a cluster config file under
`/etc/rancher/k3s/k3s.yaml` which you can merge into your machine's
`~/.kube/config` file. After changing the context with `kubens`
a `kubectl get nodes` should show your k3s node.
